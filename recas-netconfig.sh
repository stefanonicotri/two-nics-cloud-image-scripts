#!/bin/bash

# Check if the number of network interfaces is not two and decide how to behave
if [[ $(ls -A /sys/class/net | wc -l)  == 3 ]]

then

# Wait a little for DHCP lease
sleep 15

# If the private interface is on, use this command to determine its name
PRIVATE_IFACE_NAME=$(ip a | grep -E '172.30.[0-9]{1,3}' -B 2 | head -1 | awk -F\: '{print $2}' | sed "s/^ //")

# Exit if the variable is unset or empty
if [ -z "$PRIVATE_IFACE_NAME" ]
then
  echo "The variable PRIVATE_IFACE_NAME is not set" >> /var/log/recas-netconfig.log
  exit 1
fi


# If the public interface is on, use this command to determine its name
PUBLIC_IFACE_NAME=$(ip a | grep -E '90.147.[0-9]{1,3}' -B 2 | head -1 | awk -F\: '{print $2}' | sed "s/^ //")

# Exit if the variable is unset or empty
if [ -z "$PUBLIC_IFACE_NAME" ]
then
  echo "The variable PUBLIC_IFACE_NAME is not set" >> /var/log/recas-netconfig.log
  exit 2
fi

# Determine the private IP assigned by OpenStack's DHCP
PRIVATE_IP=$(hostname -I | grep -Ewo '172.30.[0-9]{1,3}.[0-9]{1,3}')

# Exit if the variable is unset or empty
if [ -z "$PRIVATE_IP" ]
then
  echo "The variable PRIVATE_IP is not set" >> /var/log/recas-netconfig.log
  exit 3
fi

# Determine the public IP assigned by OpenStack's DHCP
PUBLIC_IP=$(hostname -I | grep -Ewo '90.147.[0-9]{1,3}.[0-9]{1,3}')

# Exit if the variable is unset or empty
if [ -z "$PUBLIC_IP" ]
then
  echo "The variable PUBLIC_IP is not set" >> /var/log/recas-netconfig.log
  exit 4
fi

# Configure the private IP as static
sed -i -e "/$PRIVATE_IFACE_NAME/ s/dhcp/static/ ; /iface $PRIVATE_IFACE_NAME/a address $PRIVATE_IP\nnetmask 255.255.255.0" -e "/allow-hotplug $PRIVATE_IFACE_NAME/ s/allow-hotplug/auto/" /etc/network/interfaces.d/50-cloud-init.cfg

# Make the public interface available when restarting networking
sed -i "s/allow-hotplug/auto/" /etc/network/interfaces.d/50-cloud-init.cfg

# Default gateway to be configured
PUBLIC_GW=$(echo $PUBLIC_IP | awk -F\. '{print $1"."$2"."$3".1"}')

# Exit if the variable is unset or empty
if [ -z "$PUBLIC_GW" ]
then
  echo "The variable PUBLIC_GW is not set" >> /var/log/recas-netconfig.log
  exit 5
fi

# Current default gateway configuration
DEFAULT_GW_CONF=$(ip r | grep default)

# Exit if the variable is unset or empty
if [ -z "$DEFAULT_GW_CONF" ]
then
  echo "The variable DEFAULT_GW_CONF is not set" >> /var/log/recas-netconfig.log
  exit 6
fi

# Configure the new default gateway
ip r delete $DEFAULT_GW_CONF && ip r add default via $PUBLIC_GW dev $PUBLIC_IFACE_NAME

else
 sed -i '0,/allow-hotplug/ s/allow-hotplug/auto/' /etc/network/interfaces.d/50-cloud-init.cfg
fi
